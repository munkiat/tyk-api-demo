for conf in ../api-*.json
do 
    export YO=$(jq -r .api_definition.use_keyless $conf)
    if [ $YO == true ] 
    then
        echo "$conf is $YO"
        exit 1
    else
        echo "$conf is false"
    fi
done
